# Cornerstone, an opiniated site package companion

This package is used to avoid duplicating configuration between site package and 
define some conventions and practices to make project consistent.

This is an opiniated configuration based on our agency needs and practices, if it
fit your workflow, feels free to use it, if not, maybe you take some inspiration from
this package.

Features
--------

- [x] Explicit definition of document node contraints (by defautl a document node can not have any children)
- [x] Convention based Document + Content Fusion prototype resolving
- [x] SEO (neos/seo) + JSONLD Support (ttree/linkeddata)
- [x] Components + Styleguide friendy (ttree/componentsentinel + sitegeist/monocle)
- [x] Responsive Images (sitegeist/kaleidoscope)
- [x] Fusion Helper to include JS/CSS with cache busting (ttree/script, ttree/stylesheets)
- [x] Sentry Integration (networkteam/sentryclient)
- [x] Friendly Fusion prototype generator (medialib/fusion-prototypegenerator)
- [x] Automatic redirection when you rename a document (neos/redirecthandler)
- [x] React UI with CK Editor as the default editor (neos/ui, neos/ui-compiled)
- [x] Form handling with Swiftmailer support (neos/form, neos/swiftmailer)
- [ ] JSON based form finisher (ttree/jsonstore)
- [ ] 404 (moc/notfound)

Whislist
--------

- [ ] Code Generator


Starting a new Project
----------------------

## Basic setup

### Create a base document

In your `Configuration/NodeTypes.Overrides.yaml`:

```yaml
Ttree.Cornerstone:Document.Default:
  abstract: true
```

And create a custom default document in `NodeTypes.Document.Default.yaml`:

```yaml
Company.Website:Document.Default:
  superTypes:
    Ttree.Cornerstone:Document.Default: true
  ui:
    label: i18n
```

You must add the corresponding translations for this document type.

## Fusion setup

TODO

## JS build changin

TODO

Overriding defaults in your Site Package
----------------------------------------

TODO

Create new Document node type checklist
---------------------------------------

- [ ] TODO

Integrating Sentinel.js (Web Components)
----------------------------------------

TODO

Go live checklist
-----------------

- [ ] TODO

Acknowledgments
---------------

Development sponsored by [ttree ltd - neos solution provider](http://ttree.ch).

We try our best to craft this package with a lots of love, we are open to
sponsoring, support request, ... just contact us.

License
-------

Licensed under MIT, see [LICENSE](LICENSE)
