prototype(Ttree.Cornerstone:Content.Image) < prototype(Neos.Neos:ContentComponent) {
    imageSource = Neos.Fusion:Case {
        dummy {
            condition = ${q(node).property('image') == null}
            renderer = Sitegeist.Kaleidoscope:DummyImageSource
        }
        default {
            condition = true
            renderer = Sitegeist.Kaleidoscope:AssetImageSource {
                asset = ${q(node).property('image')}
            }
        }
    }

    alt = ${q(node).property('alternativeText')}
    title = ${q(node).property('title')}

    hasCaption = ${q(node).property('hasCaption')}
    caption = Neos.Neos:Editable {
        property = 'caption'
    }

    srcset = "320w, 400w, 600w, 800w, 1000w, 1200w, 1600w"
    sizes = "(min-width: 800px) 1000px, (min-width: 480px) 800px, (min-width: 320px) 440px, 100vw"

    renderer = afx`
        <figure class="ImageWrapper">
            <Sitegeist.Kaleidoscope:Image
                imageSource={props.imageSource}
                alt={props.alt}
                title={props.title}
                srcset={props.srcset}
                sizes={props.sizes}
                />
            <figcaption @if.hasCaption={props.hasCaption}>
                {props.caption}
            </figcaption>
        </figure>
    `
}
